# Build Docker container for plast and bifrost
FROM ubuntu:22.04
RUN apt-get update && apt-get install -y bash build-essential git cmake zlib1g-dev
WORKDIR /usr/local/src
RUN git clone https://github.com/pmelsted/bifrost.git
WORKDIR /usr/local/src/bifrost/build
RUN cmake ..	
RUN make
RUN make install
WORKDIR /usr/local/src
RUN git clone --branch bifrost1.2.1 https://gitlab.ub.uni-bielefeld.de/gi/plast.git
WORKDIR /usr/local/src/plast/src/
RUN make
RUN cp /usr/local/src/plast/src/PLAST /usr/bin/
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/